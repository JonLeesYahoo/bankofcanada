package com.rboc.retail.online;

import java.math.BigDecimal;
import java.util.Map;

public class Checkout {

	/**
	 * Total up all the BigDecimal values in the shopping Cart 
	 * 
	 * @param shoppingCart - the collection of all the items to add up
	 * @return - the total value of all the items 
	 */
	public BigDecimal getTotal(Map<String, BigDecimal> shoppingCart) {
		return shoppingCart.values().stream().reduce(BigDecimal.ZERO, BigDecimal::add);
	}
}
