package com.rboc.retail.online;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestCheckout {

	private Map<String, BigDecimal> shoppingCart = new HashMap<String, BigDecimal>();
	
	@Before 
	public void setUp () {
		//Add any of the following; Bananas, Oranges, Apples, Lemons, Peaches
		shoppingCart.put("Banana", new BigDecimal(1.4));
		shoppingCart.put("Lemon", new BigDecimal(1.2));
		shoppingCart.put("Peaches", new BigDecimal(1.5));
	}

	@Test
	public void testCheckoutTotalsCorrectly() {
		Checkout checkout = new Checkout();
		BigDecimal total = checkout.getTotal(shoppingCart);
		Assert.assertEquals(Math.round(4.1), Math.round(total.doubleValue()));
	}
	
}
